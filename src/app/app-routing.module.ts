import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoanCalculatorComponent } from './pages/loan-calculator/loan-calculator.component';

const routes: Routes = [{
  path: 'loan-calculator', component: LoanCalculatorComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
