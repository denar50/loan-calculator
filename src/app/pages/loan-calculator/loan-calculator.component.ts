import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms"
import { BankClient } from 'src/clients/bank';

enum Children {
  NONE = "NONE",
  SINGLE = "SINGLE",
  MULTIPLE = "MULTIPLE"
}

enum Coapplicant {
  NONE = "NONE",
  SINGLE_BORROWER = "SINGLE_BORROWER",
  MULTIPLE_BORROWERS = "MULTIPLE_BORROWERS"
}

const minMonthlyIncome = 500;
const minRequestAmount = 20000;
const minLoanTerm = 3;
const maxLoanTerm = 30;

@Component({
  selector: 'app-loan-calculator',
  templateUrl: './loan-calculator.component.html',
  styleUrls: ['./loan-calculator.component.css']
})
export class LoanCalculatorComponent {
  calculatorForm: FormGroup
  minLoanTerm: number
  maxLoanTerm: number
  minRequestedAmount: number
  minMonthlyIncome: number

  childrenFormTypes = [Children.NONE, Children.SINGLE, Children.MULTIPLE]
  coapplicantFormTypes = [Coapplicant.NONE, Coapplicant.SINGLE_BORROWER, Coapplicant.MULTIPLE_BORROWERS]

  bankClient: BankClient

  getChildrenTypeLabel(type: Children) {
    return type.toLowerCase()
  }

  getCoapplicantTypeLabel(type: Coapplicant) {
    return type.toLowerCase()
  }

  async calculateLoan() {
    try {
      const response = await this.bankClient.calculateLoan(this.calculatorForm.value)
      console.log("Congratulations! Your approved stuff is " + JSON.stringify(response))
    }catch(error) {
      console.log(JSON.stringify(error))
      alert("Something went wrong. Try again")
    }    
  }

  constructor() {
    const onlyIntegerValidator = Validators.pattern("^[0-9]*$")
    this.minLoanTerm = minLoanTerm
    this.maxLoanTerm = maxLoanTerm
    this.minRequestedAmount = minRequestAmount
    this.minMonthlyIncome = minMonthlyIncome

    this.calculatorForm = new FormGroup({
      monthlyIncomeControl: new FormControl('', [Validators.required, onlyIntegerValidator, Validators.min(minMonthlyIncome)]),
      requestedAmountControl: new FormControl('', [Validators.required, onlyIntegerValidator, Validators.min(minRequestAmount)]),
      loanTermControl: new FormControl(minLoanTerm, [Validators.required, onlyIntegerValidator, Validators.min(minLoanTerm), Validators.max(maxLoanTerm)]),
      childrenControl: new FormControl(Children.NONE),
      coapplicantControl: new FormControl(Coapplicant.NONE),
    })

    // TODO: bring this from a configuration file
    this.bankClient = new BankClient({ url: "https://homework.fdp.workers.dev/", apiKey: "swb-222222" })
  }
}
