import { object, nonEmptyString, url, integer } from 'decoders'
interface BankClientConfig {
    url: URL,
    apiKey: string
}

enum Children {
    NONE = "NONE",
    SINGLE = "SINGLE",
    MULTIPLE = "MULTIPLE"
}

enum Coapplicant {
    NONE = "NONE",
    SINGLE_BORROWER = "SINGLE_BORROWER",
    MULTIPLE_BORROWERS = "MULTIPLE_BORROWERS"
}

interface CalculateLoanParameters {
    monthlyIncome: number,
    requestedAmount: number,
    loanTerm: number,
    children: Children,
    coapplicant: Coapplicant
}

interface CalculateLoanResponse {
    loanAmount: number,
    interestRate: number
}

const clientConfigDecoder = object({
    url,
    apiKey: nonEmptyString
})

const calculateLoanResponseDecoder = object({
    loanAmount: integer,
    interestRate: integer
})

export class BankClient {
    private config: BankClientConfig;
    constructor(config: unknown) {
        this.config = clientConfigDecoder.verify(config);
    }

    async calculateLoan(params: CalculateLoanParameters): Promise<CalculateLoanResponse> {
        try {
            const response = await fetch(this.config.url, {
                method: "POST",
                mode: 'cors',
                body: JSON.stringify(params),
                headers: {
                    'Content-Type': 'application/json',
                    'X-API-KEY': this.config.apiKey
                }
            });
            return calculateLoanResponseDecoder.verify(response.json())
        } catch (error) {
            // TODO: add error handling
            throw error;
        }
    }
}